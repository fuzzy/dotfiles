set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'mg979/vim-visual-multi'
Plugin 'terrortylor/nvim-comment'
Plugin 'windwp/nvim-autopairs'
Plugin 'Pocco81/AutoSave.nvim'
Plugin 'karb94/neoscroll.nvim'
Plugin 'nvim-treesitter/nvim-treesitter'
Plugin 'itchyny/lightline.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'norcalli/nvim-colorizer.lua'
Plugin 'lukas-reineke/indent-blankline.nvim'
Plugin 'neovim/nvim-lspconfig'
Plugin 'easymotion/vim-easymotion'
Plugin 'neoclide/coc.nvim'

Plugin 'sainnhe/sonokai'
Plugin 'chasinglogic/modus-themes-vim'
Plugin 'joshdick/onedark.vim'
Plugin 'https://gitlab.com/yorickpeterse/happy_hacking.vim'
Plugin 'sickill/vim-monokai'
Plugin 'sonph/onehalf'
Plugin 'jnurmine/Zenburn'
Plugin 'drewtempelmeyer/palenight.vim'
Plugin 'morhetz/gruvbox'
Plugin 'wadackel/vim-dogrun'
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'bluz71/vim-moonfly-colors'
Plugin 'tyrannicaltoucan/vim-deep-space'
Plugin 'xero/sourcerer.vim'
Plugin 'tssm/fairyfloss.vim'
Plugin 'catppuccin/nvim'

call vundle#end()
filetype plugin indent on

"set termguicolors
lua require('neoscroll').setup()
lua require('nvim-autopairs').setup()
lua require('nvim_comment').setup()
"lua require('colorizer').setup()

""" Main Config
colorscheme sourcerer
set tabstop=8 softtabstop=8 noexpandtab smarttab autoindent
set incsearch ignorecase smartcase hlsearch
set ruler laststatus=2 showcmd showmode
set wrap breakindent
set encoding=utf-8
set textwidth=0
set hidden
set number relativenumber
set title
set cursorline
"hi CursorLine cterm=bold
"hi CursorLineNr term=bold cterm=bold
"hi LineNr
"hi Whitespace ctermbg=red
match Whitespace /\s\+$/

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
" use <c-space>for trigger completion
inoremap <silent><expr> <c-space> coc#refresh()
" Use <C-@> on vim
inoremap <silent><expr> <c-@> coc#refresh()
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"
