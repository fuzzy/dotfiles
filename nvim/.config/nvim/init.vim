filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'mg979/vim-visual-multi'
Plugin 'terrortylor/nvim-comment'
Plugin 'windwp/nvim-autopairs'
Plugin 'Pocco81/AutoSave.nvim'
Plugin 'karb94/neoscroll.nvim'
Plugin 'nvim-treesitter/nvim-treesitter'
Plugin 'itchyny/lightline.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'norcalli/nvim-colorizer.lua'
Plugin 'lukas-reineke/indent-blankline.nvim'
Plugin 'neovim/nvim-lspconfig'
Plugin 'easymotion/vim-easymotion'
Plugin 'neoclide/coc.nvim'

Plugin 'sainnhe/sonokai'
Plugin 'chasinglogic/modus-themes-vim'
Plugin 'joshdick/onedark.vim'
Plugin 'https://gitlab.com/yorickpeterse/happy_hacking.vim'
Plugin 'sickill/vim-monokai'
Plugin 'sonph/onehalf'
Plugin 'jnurmine/Zenburn'
Plugin 'drewtempelmeyer/palenight.vim'
Plugin 'morhetz/gruvbox'
Plugin 'altercation/vim-colors-solarized'
Plugin 'wadackel/vim-dogrun'
Plugin 'tyrannicaltoucan/vim-quantum'
Plugin 'bluz71/vim-moonfly-colors'
Plugin 'tyrannicaltoucan/vim-deep-space'
Plugin 'xero/sourcerer.vim'
Plugin 'arzg/vim-corvine'
Plugin 'tssm/fairyfloss.vim'
Plugin 'catppuccin/nvim'

call vundle#end()
filetype plugin indent on

nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>
"set termguicolors
lua require('neoscroll').setup()
lua require('nvim-autopairs').setup()
lua require('nvim_comment').setup()
"lua require('colorizer').setup()

""" Main Config
colorscheme sourcerer
set tabstop=8 softtabstop=8 noexpandtab smarttab autoindent
set incsearch ignorecase smartcase hlsearch
set ruler laststatus=2 showcmd showmode
set wrap breakindent
set encoding=utf-8
set textwidth=0
set hidden
set number relativenumber
set title
set cursorline
"hi CursorLine cterm=bold
"hi CursorLineNr term=bold cterm=bold
"hi LineNr
"hi Whitespace ctermbg=red
match Whitespace /\s\+$/

set statusline+=%#warningmsg#
set statusline+=%*

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
	" Recently vim can merge signcolumn and number column into one
	set signcolumn=number
else
	set signcolumn=yes
endif

" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()

" use <c-space>for trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
