#!/bin/fish
set -Ux HOME /home/fuzzy
set -Ux XDG_CONFIG_HOME /home/fuzzy/.config
set -Ux XDG_DATE_HOME /home/fuzzy
set -Ux XDG_CACHE_HOME /home/fuzzy/.cache
set -gx	GPG_TTY (tty)
alias rm="rm -dr"
alias cp="cp -r"
alias exa="exa -aF -s type"
alias mkdir="mkdir -p"
alias hima="himalaya"
alias disroot="himalaya -a disroot"
alias maim="maim ~/Pictures/screenshots/(date +%s).png"
alias sxiv="nsxiv -af -e (xdotool getwindowfocus) -g (/home/fuzzy/Documents/scripts/getWindowGeometry.sh)"
alias nnnp="NNN_FIFO=/tmp/nnn.fifo NNN_PLUG="p:preview-tabbed" nnn"
alias transmission-remote="transmission-remote -n fuzzy:comma"

fish_add_path -g /bin /usr/bin /usr/local/bin /home/fuzzy/.cargo/bin /home/fuzzy/.local/bin /home/fuzzy/.local/bin 

# Start X at login
if status is-interactive
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end
