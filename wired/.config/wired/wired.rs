(
	max_notifications: 0,
	timeout: 3000,
	poll_interval: 16,
	replacing_enabled: true,
	closing_enabled: true,
	debug: false,
	debug_color: Color(r: 0.0, g: 1.0, b: 0.0, a: 1.0),
	debug_color_alt: Color(r: 1.0, g: 0.0, b: 0.0, a: 1.0),

	shortcuts: ShortcutsConfig(	
		notification_close: 1,
		notification_interact: 2,
		notification_action1: 3,
	),

	layout_blocks: [
		(
			name: "root",
			parent: "",
			hook: Hook(parent_anchor: TR, self_anchor: TR),
			offset: Vec2(x: -7.0, y: 35.0),

			params: NotificationBlock((
				monitor: 2,
				border_width: 3.0,
				border_rounding: 3.0,
				background_color: Color(hex: "#181819"),
				border_color: Color(hex: "#181819"),
				border_color_low: Color(hex: "#181819"),	
				border_color_critical: Color(hex: "#181819"),
				border_color_paused: Color(hex: "#181819"),
	
				gap: Vec2(x:0.0, y:8.0),
				notification_hook: Hook(parent_anchor: BR, self_anchor: TR),
			)),
		),
		(
			name: "summary",
			parent: "root",
			hook: Hook(parent_anchor: TL, self_anchor: TL),
			offset: Vec2(x: 0.0, y:0.0),
			params: TextBlock((
				text: "%s",
				font: "SF Pro Bold 12",
				ellipsize: NoEllipsize,
				color: Color(hex: "#d3d3d3"),
				padding: Padding(left: 7.0, right: 7.0, top: 7.0, bottom: 0.0),
				dimensions: (width: (min: 300, max: 300), height: (min: 0, max: 0)),
			)),

		),
		(
			name: "body",
			parent: "summary",
			hook: Hook(parent_anchor: BL, self_anchor: TL),
			offset: Vec2(x: 0.0, y: -3.0),

			params: TextBlock((
				text: "%b",
				font: "SF Pro 12",
				color: Color(hex: "#d3d3d3"),
				ellipsize: NoEllipsize,
				padding: Padding(left: 7.0, right: 7.0, top: 3.0, bottom: 7.0),
				dimensions: (width: (min: 300, max: 300), height: (min: 0, max: 0)),
			)),
		),
	
	],
)
