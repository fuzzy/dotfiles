;=====================================================================
;
;
; ███████╗██╗   ██╗███████╗███████╗██╗   ██╗██████╗  █████╗ ██████╗ 
; ██╔════╝██║   ██║╚══███╔╝╚══███╔╝╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
; █████╗  ██║   ██║  ███╔╝   ███╔╝  ╚████╔╝ ██████╔╝███████║██████╔╝
; ██╔══╝  ██║   ██║ ███╔╝   ███╔╝    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
; ██║     ╚██████╔╝███████╗███████╗   ██║   ██████╔╝██║  ██║██║  ██║
; ╚═╝      ╚═════╝ ╚══════╝╚══════╝   ╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;		To learn more about how to configure Polybar
;   		   go to https://github.com/jaagr/polybar
;
;		  The README contains alot of information
;
;===================================================================

[colors]
background =	 #CC222222
background-alt = #E6212121
foreground =	 #c2c2b0
foreground-alt = #a5a5a5

[bar/fuzzybar]
monitor = DP-2

width =		100%
height =	30

fixed-center = 	true
bottom =     	false

background = ${colors.background}
foreground = ${colors.foreground}

border-size =	0
border-color = 	#09000000

padding-left =	2
padding-right = 2

module-margin-left = 1
module-margin-right = 1

font-0 = SF Pro Display:weight=SemiBold:pixelsize=12.5;3
font-1 = DejaVuSansMono Nerd Font:pixelsize=13;3
;https://dejavu-fonts.github.io/
;https://www.nerdfonts.com/font-downloads
font-2 = forkawesome:pixelsize=13;3
;https://forkaweso.me/Fork-Awesome/
font-3 = Source Han Sans JP VF:pixelsize=13;3

modules-left   = moon title 
modules-center = swatch
modules-right  = network clipboard soundSwitch soundIcon pulseaudio newpower

tray-position 	  = right
tray-padding  	  = 3

[module/datetime]
type=internal/date
interval = 5

date = %a %b %d

time = %l:%M%P

label = %date% %time%

click-left = dcal

[module/swatch]
type=custom/script
exec = bash ~/Documents/scripts/bartime.sh
tail = true
format = <label>
label = %output%

click-left = ~/Documents/scripts/dcaltoggle.sh

[module/moon]
type=custom/script
exec = moonmoji
format = <label>
label = %output%

[module/clipboard]
type=custom/text

content="  "
click-left = ~/Documents/scripts/xclipmenu.sh

[module/soundSwitch]
type=custom/text

content=""
click-left = ~/Documents/scripts/soundSwitch.sh

[module/soundIcon]
type=custom/script
exec = bash ~/Documents/scripts/soundSwitchIcon.sh

interval = 0
label=%output%

[module/pulseaudio]
type = internal/pulseaudio
format-volume = <ramp-volume> <label-volume> <bar-volume>
ramp-volume-0 = 奄
ramp-volume-1 = 奔
ramp-volume-2 = 墳
ramp-volume-3 = 墳
ramp-volume-4 = 墳
label-volume =  %percentage%%
labelvolume-foreground = ${root.foreground}
ramp-volume-padding = 1
label-muted = 婢  muted %{F#b70e17}─────────%{F-}
label-muted-foreground = #ffffff

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 0
bar-volume-fill = ─ 
bar-volume-fill-font = 0
bar-volume-empty = ─
bar-volume-empty-font = 0
bar-volume-empty-foreground = ${colors.foreground-alt}

interval = 2

[module/title]
type=internal/xwindow

format = <label>

label = %title%
label-maxlen=50

label-empty =
label-empty-foreground = ${colors.background}

[module/network]
type = internal/network
interface = enp0s31f6
unknown-as-up = true

interval = 5.0
upspeed-min = 0
downspeed-min = 0

format-connected = <label-connected>
label-connected =  %downspeed%  %upspeed% 

format-disconnected = <label-disconnected>
label-disconnected = "no network"

[module/newpower]
type=custom/text

content = 

click-left =~/Documents/scripts/xpowermenu.sh 

[module/ewmh]
type = internal/xworkspaces

pin-workspaces = false

enable-click = true

enable-scroll = false

format = <label-state>

[settings]
screenchange-reload = true
